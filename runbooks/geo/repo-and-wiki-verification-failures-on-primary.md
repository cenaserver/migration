# Resolve repo & wiki verification failures on the primary

## First and foremost

*Don't Panic*

## Diagnose errors

- Visit [Kibana](https://log.gitlab.net/app/kibana)
- Select `pubsub-rails-inf-gprd`
- For failures on the primary, search for `"Error calculating the repository checksum"`

### How to enable/disable verification

```ruby
# Enable verification
Feature.enable('geo_repository_verification')

# Disable verification
Feature.disable('geo_repository_verification')

# Check setting
Gitlab::Geo.repository_verification_enabled?
```

Don't forget to enable/disable on the secondary as well!

### Export verification failures to CSV

```ruby
# Repositories that failed verification on a primary
repo_failures = ProjectRepositoryState.verification_failed_repos

data = CSV.open('/tmp/repo_verification_failures.csv', 'w')
repo_failures.each do |fail|
   data << [fail.project_id, fail.last_repository_verification_failure&.gsub("\n", " ")]
end; nil
data.close
```

```ruby
# Wikis that failed verification on a primary
wiki_failures = ProjectRepositoryState.verification_failed_wikis

data = CSV.open('/tmp/wiki_verification_failures.csv', 'w')
wiki_failures.each do |fail|
   data << [fail.project_id, fail.last_wiki_verification_failure&.gsub("\n", " ")]
end; nil
data.close
```

### Get count of repos that failed verification on a primary

```ruby
ProjectRepositoryState.verification_failed_repos.count
```

### Get count of wikis that failed verification on a primary

```ruby
ProjectRepositoryState.verification_failed_wikis.count
```

## Recalculate checksums on the primary

If you have a list of project IDs whose checksums you'd like to recalculate:

```ruby
# Repository
project_states = ProjectRepositoryState.where(project_id: project_ids)
project_states.update_all(repository_verification_checksum: nil, last_repository_verification_failure: nil)

# Wiki
project_states = ProjectRepositoryState.where(project_id: project_ids)
project_states.update_all(wiki_verification_checksum: nil, last_wiki_verification_failure: nil)
```
