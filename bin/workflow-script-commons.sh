#!/usr/bin/env bash

# Used for sanity checking the scripts
if [[ -n ${SANITY_CHECK_ONLY:=} ]]; then
  exit 0
fi

# Everything is logged!
if [[ -z ${LOGGING_CONFIGURED:=} ]]; then
  export LOGGING_CONFIGURED=1
  # TODO(andrewn): once we have a destination, we can also tap these logs to another place
  $0 "$@"  2>&1 | ruby -pe 'print Time.now.strftime("%Y-%m-%d %H:%M:%S.%L: ")'
  exit
fi

function die() {
  >&2 echo "Fatal:" "$@"
  exit 1
}

function ssh_host() {
  # shellcheck disable=SC2068
  ssh -o StrictHostKeyChecking=no $@
}

function PRODUCTION_ONLY() {
  if [[ $FAILOVER_ENVIRONMENT != "prd" ]]; then
    die "This step is production only"
  fi
}

# Basic support for OSX, helpful for testing
function gnu_date() {
  if command -v gdate >/dev/null; then
    gdate "$@"
  else
    date "$@"
  fi
}

# Basic support for OSX, helpful for testing
function gnu_readlink() {
  if command -v greadlink >/dev/null; then
    greadlink "$@"
  else
    readlink "$@"
  fi
}

function ensure_valid() {
  source_vars_file=$(gnu_readlink -f "${SOURCE_VARS_DIR}/source_vars")
  grep -Eho '(\w+)="__REQUIRED__"' "${SOURCE_VARS_DIR}/source_vars_template.sh" |cut -d= -f1 | while read -r i; do
    if [[ ${!i:=__REQUIRED__} = "__REQUIRED__" ]]; then
      die "Variable ${i} has not been configured. You may need to update ${source_vars_file}"
    fi
  done

  case ${FAILOVER_ENVIRONMENT} in
    "prd") ;;
    "stg") ;;
    *) die "Unknown environment. Must be 'prd' or 'std': update ${source_vars_file}";;
  esac

  FAILOVER_DATE=$(gnu_date --date="$FAILOVER_DATE" "+%Y-%m-%d")
  TODAY=$(gnu_date "+%Y-%m-%d")
  if [[ "${FAILOVER_DATE}" < "${TODAY}" ]]; then
    die "Failover date is in the past ${FAILOVER_DATE}. Have you updated ${source_vars_file}?"
  fi

  case $(hostname -f) in
    "deploy-01-sv-gprd.c.gitlab-production.internal")
      if [[ ${FAILOVER_ENVIRONMENT} != "prd" ]]; then
        die "FAILOVER_ENVIRONMENT is ${FAILOVER_ENVIRONMENT}, but environment is detected as production. Have you updated ${source_vars_file}?"
      fi
      ;;
    "deploy-01-sv-gstg.c.gitlab-staging-1.internal")
      if [[ ${FAILOVER_ENVIRONMENT} != "stg" ]]; then
        die "FAILOVER_ENVIRONMENT is ${FAILOVER_ENVIRONMENT}, but environment is detected as staging. Have you updated ${source_vars_file}?"
      fi
      ;;
    *)
      if [[ ${SKIP_HOST_CHECK:=} != "true" ]]; then
        case ${FAILOVER_ENVIRONMENT} in
          "prd") die "Unrecognised $(hostname -f): please run this from the GCP deploy host: deploy-01-sv-gprd.c.gitlab-production.internal. Set SKIP_HOST_CHECK=true if you know what you're doing." ;;
          "stg") die "Unrecognised $(hostname -f): please run this from the GCP deploy host: deploy-01-sv-gstg.c.gitlab-staging-1.internal. Set SKIP_HOST_CHECK=true if you know what you're doing." ;;
          *) die "Unrecognised $(hostname -f). Please review ${source_vars_file}" ;;
        esac
      fi
      ;;
  esac

  if [[ -z "${SSH_AUTH_SOCK:=}" ]]; then
    >&2 echo "Warning: you appear to not have SSH agent forwarding setup. You may need to add \`ForwardAgent yes\` to your ~/.ssh/config"
  fi
}

function log_command() {
  (set -x; "$@")
}

function remote_gitlab_runner() {
  host=$1
  script=$2
  temp="$(ssh "${host}" mktemp).rb"
  scp "$script" "${host}:${temp}"
  log_command ssh "${host}" bash -c "set -x; sudo gitlab-rails runner \"\$(cat ${temp})\" && rm ${temp}"
}

function header() {
  local full_path
  full_path=$(gnu_readlink -f "${BASH_SOURCE[2]}")
  cat <<EOD
========================================================
Script: $full_path
User: ${SUDO_USER:=$USER}
Rev: $(git -C "$(dirname "${BASH_SOURCE[0]}" )/.." rev-parse --short HEAD)
========================================================

EOD
}

function footer() {
  cat <<EOD

--------------------------------------------------------
Exit Status: $?
--------------------------------------------------------
EOD
}

header
trap "footer" EXIT

SOURCE_VARS_DIR=$(dirname "${BASH_SOURCE[0]}")

if ! [[ -f "${SOURCE_VARS_DIR}/source_vars" ]]; then
  target=$(gnu_readlink -f "${SOURCE_VARS_DIR}/source_vars")
  source=$(gnu_readlink -f "${SOURCE_VARS_DIR}/source_vars_template.sh")

  die "${target} not found. Please initialise by with 'cp ${source} ${target}'"
fi

# Load the defaults
# shellcheck disable=SC1091,SC1090
source "${SOURCE_VARS_DIR}/source_vars_template.sh"

# Load the specific values
# shellcheck disable=SC1091,SC1090
source "${SOURCE_VARS_DIR}/source_vars"

ensure_valid

